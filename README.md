# Respuestas a test Awto - Felipe Piña Bandera

# Introducción

¡Hola! Espero que estén bien.

Cada una de las partes de la tarea está separada en un archivo de jupyter notebook, con su correspondientes resultados de ejecución, instrucciones de compilación básicas y los supuestos considerados para su resolución.


## Caso de aplicación (Final countdown).

_Navegue por la app de awto y su solución para entender brevemente el producto. Segun sus recomendaciones de experto e innovador en materia de DS, proponga una forma de implementar mecanismos de machine learning o modelos predictivos, que permitan mejorar la experiencia de los usuarios y su conversión. Para este fin, establesca los supuestos que crea necesario y argumente su propuesta de forma tanto teorica, como tecnica, indicando su impacto en el negocio_

Cuando me propuse navegar en la aplicación, se me ocurrieron varias cosas antes de pensar en lo analítico. Entre ellas, que varias cabezas piensan mejor que una y que vivenciar la aplicación con una mirada crítica desde la mirada del cliente es lo más importante. Considerando lo anterior, le pedí a una amiga que nunca había usado Awto que me acompañara en un viaje, así que les iré comentando mis reflexiones de experiencia de usuario y analítica en algunos breves párrafos.

La navegación por la aplicación es sencilla y fácil de usar en la pestaña principal. Cabe destacar que yo sí había usado Awto antes, por lo que la sección de introducir las credenciales personales no fue visitada.

Para reservar un coche, tenemos la primera restrcción que es de poca disponibilidad de autos en la zona céntrica. Acá se puede pensar en un modelo de propensión de desplazamiento de potenciales clientes, considerando el grupo socioeconómico de las zonas de residencia de los mismos (si es que está disponible esa información) y así proveer de más vehículos en nuevas zonas.

En esta parte, también podría estar implementado un modelo de ruta óptima junto a un simulador para obtenter un tiempo y costo estimado en el viaje deseado. En resumen, si simulo que elijo un Awto determinado como origen y un estacionamiento de destino, quisiera saber cuánto tiempo me demoro en hacer mi ruta y cuánta plata me saldrá, para así poder compararlo con solo un par de toques de pantalla. Si me conviene, uso un Awto en vez de la competencia :D

Desde la experiencia de usuario:
- está cubierto el caso en que madres y padres vayan con su hijos pequeños, ¿se dispone de silla de automóvil?.
- El tema de las mascotas también. También podrían salir emoticones con información adicional en la pantalla de selección del vehículo (un icono de perro o bebé, etc).

Tras reservar el coche, previo a la ruta, consideré unas cosas de experiencia de usuario que son importantes de mencionar:

- Al reportar daños el vehículo, se tienen que enviar todos los hitos y fotos de una vez para todos los temas. De forma intuitiva, pensé en que si llenaba un hito ("rayón) agregando una foto correspondiente y enviándola, la app me ofrecería volver a agregar otro hito, pero da por hecho que lo agregué todo.
- Es común reportar los daños exteriores fuera del vehículo, pero si quiero reportar que el auto está sucio, como ya mandé el hito de auto dañado, no lo puedo agregar después.

De cara al viaje, la actualización de la ruta en el mapa, considerando la ubicación actual, no es de las mejores. Desconozco si el modelo que genera la ruta lo hace a través de un optimizador de la misma considerando niveles de tráfico o similar.

En la etapa de cierre del viaje, de cara a la experiencia de usuario:

- cerré el viaje con las puertas abiertas, quedando abierto el auto. Como no encontré rápidamente un teléfono al cual llamar, tuve que reservar el mismo auto para pensar en el cierre de puertas. Finalmente, al reservar el mismo auto, apareció un número para llamar a un callcenter en donde pude hacerlo.
- no necesariamente los estacionamientos están demarcados desde la entrada al recinto.
- en ninguna parte del viaje se mencionó que, en aquellos lugares en donde se accede con un ticket de estacionamiento, hay que dejarlo dentro del auto.

De cara a algo más general relativo al negocio de Awto, pienso en:
- modelo de retención de clientes, para evitar la fuga.
- modelo de propensión de tendencia de daños y robos de vehículos
- proponer una rama de un modelo de negocios tipo Uber pero usando los Awtos. Es decir, seleccionar choferes para que muevan los awtos por determinadas horas y así seguir masificando la marca.

Como consideración final, pienso que las oportunidades de analítica y de experiencia de usuario levantadas pueden potenciar el uso de la aplicación y de la toma de decisiones basadas en datos.